let h1_player = document.getElementsByClassName("suit_player");
let h1_com = document.getElementsByClassName("suit_com");
let btnReset = document.getElementById("btnReset");
let txtInfo = document.getElementsByClassName("txt-info")[0];
let txtScorePlayer = document.getElementsByClassName("score_player")[0];
let txtScoreCom = document.getElementsByClassName("score_com")[0];
let picked_com;
let score_player = 0;
let score_com = 0;
let array_jakepon = ["batu", "kertas", "gunting"];
let beep_sound = "assets/beep.mp3";
let refresh_sound = "assets/negatif.mp3";

let beepSound = (src) => {
    let sound = document.createElement("audio");
    sound.src = src;
    sound.play();
  }

let random_suit = () => { 
    let i = Math.floor(Math.random() * 3);
    console.log("com" + ': ' + array_jakepon[i]);
    return i;
}

let picked = () => {
    for (let i = 0; i < h1_player.length; i++) {
        h1_player[i].addEventListener("click", event => {
            refresh();
            beepSound(beep_sound);
            picked_com = random_suit();
            console.log("player" + ': ' +array_jakepon[i]);
            h1_player[i].classList.add("kotak_picked");
            h1_com[picked_com].classList.add("kotak_picked");
            if (tos(array_jakepon[i], array_jakepon[picked_com]) == "win") {
                count_score("win");
            } else if(tos(array_jakepon[i], array_jakepon[picked_com]) == "lose") {
                count_score("lose");
            } else {
                count_score("draw");
            }
        });
    }
    reset();
}

let count_score = (result) => {
    if (result == "win") {
        console.log("result : win");
        score_player++;
        txtScorePlayer.innerHTML = score_player;
        console.log("player : " + score_player);
        txtInfo.innerHTML = "Win !";
    } else if (result == "lose") {
        console.log("result : lose");
        txtInfo.innerHTML = "Lose !";
        score_com++;
        txtScoreCom.innerHTML = score_com;
        console.log("com : " + score_com);
    } else {
        console.log("result : draw");
        txtInfo.innerHTML = "Draw !";
    }
    picked_com = random_suit();
} 

let refresh = () => {
    for( let i = 0; i < h1_player.length; i++){
        h1_player[i].classList.remove("kotak_picked");
        h1_com[i].classList.remove("kotak_picked");
    }
}

let reset = () => {
    btnReset.addEventListener("click", event => {
        console.log("reset clicked");
        for (let i = 0; i < h1_player.length; i++) {
            h1_player[i].classList.remove("kotak_picked");
            h1_com[i].classList.remove("kotak_picked");
        };
        console.log("reseted");
        txtInfo.innerHTML = "START GAME";
        score_player = 0;
        score_com = 0;
        txtScoreCom.innerHTML = score_com;
        txtScorePlayer.innerHTML = score_player;
        picked_com = random_suit();
        beepSound(refresh_sound);
    });
}

let tos = (player, com) => {
    let hasil;
    switch (player) {
        case "batu":
            if(com == "gunting"){
                hasil = "win";
            } else if (com == "kertas") {
                hasil = "lose"
            } else{
                hasil = "draw"
            }
            break;
        case "gunting":
            if(com == "gunting"){
                hasil = "draw";
            } else if (com == "kertas") {
                hasil = "win"
            } else{
                hasil = "lose"
            }
            break;
        case "kertas":
            if(com == "gunting"){
                hasil = "lose";
            } else if (com == "kertas") {
                hasil = "draw"
            } else{
                hasil = "win"
            }
            break;
    }
    return hasil
}

let loadData = () => {
    let req = new XMLHttpRequest();

    req.open('GET', "http://localhost:8000/api/v2/assets", true);
    let data = [];

    req.onload = function() {
        let load = JSON.parse(this.response)
        data2 = load.data;
        let i = 0;
        if (req.status >= 200 && req.status < 400) {
            data2.forEach((assets) => {
                let j = i++
                h1_com[j].src = assets.url
                h1_player[j].src = assets.url
                data.push(assets)
            })
        } else {
            console.log(load.status)
        }
        console.log('allready')
    }
    req.send()
}
loadData()
picked();